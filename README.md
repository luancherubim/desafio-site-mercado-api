# Desafio Site Mercado WebApi

# Ver Demonstração

https://desafio-site-mercado.azurewebsites.net

## Rodar Local

1 - Faça o download deste repositório e inicie o projeto.

2 - Criar Banco de Dados "Products"

Pordemos criar o banco de duas formas:

Opção 01) Usando Migration (Recomendável)

No Package Manager Console Visual Studio execute este comando para fazer o migration:

Add-Migration FirstMigration -Context ProductContext

Depois este para atualizar o banco:

Update-Database -Context ProductContext

ou,

Opção 02) Utilizando Script SQL

CREATE DATABASE Products;

USE Products;
CREATE TABLE Products(
    ProductId int IDENTITY(1,1) NOT NULL,
    Name varchar(max) NULL,
    Price real NOT NULL,
    Image varchar(max) NULL
);

3 - Se for necessário, ajuste o DefaultConnection em "appsetings.json" com a credencial de seu banco.

{
    "ConnectionStrings": {
    "DefaultConnection": "Data Source=.\\SQLEXPRESS;Integrated Security=true;Initial Catalog=Products;MultipleActiveResultSets=true;Trusted_Connection=True;"
}

4 - Play

Por padrão a url da api é: https://localhost:44338

Caso o projeto esteja rodando com outra url, lembre-se de alterar a variavel de ambiente no Aplicação Angular em  src/environments apiUrl (https://bitbucket.org/luancherubim/desafio-site-mercado-angular)


# Desafio Site Mercado WebApi (ENG)

# Run Demo

https://desafio-site-mercado.azurewebsites.net

## Run on Local server

1 - Download this repository.

2 - Create Database "Products"

There are two options to create db:

Option 01) Using Migration

On Visual Studio 2019 Package Manager Console insert this command and hit enter:

Add-Migration FirstMigration -Context ProductContext

Then:

Update-Database -Context ProductContext

or try,

Option 02) Using SQL Script

CREATE DATABASE Products;

USE Products;
CREATE TABLE Products(
    ProductId int IDENTITY(1,1) NOT NULL,
    Name varchar(max) NULL,
    Price real NOT NULL,
    Image varchar(max) NULL
);

3 - Configure this line on "appsetings.json" with your database credencials.

{
    "ConnectionStrings": {
    "DefaultConnection": "Data Source=.\\SQLEXPRESS;Integrated Security=true;Initial Catalog=Products;MultipleActiveResultSets=true;Trusted_Connection=True;"
}

4 - Play

Default Url: https://localhost:44338

If Api doesn't run on default url, change the apiUrl variable on evironments on Angular App (https://bitbucket.org/luancherubim/desafio-site-mercado-angular).