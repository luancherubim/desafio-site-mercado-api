﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using web_api.Models;

namespace web_api.Controllers
{
    [Route("api/product")]
    [ApiController]
    public class ProductController : Controller
    {
        private ProductContext _productContext;

        public ProductController(ProductContext context)
        {
            _productContext = context;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<Product>> Get()
        {
            return _productContext.Products.ToList();
        }

        [HttpGet]
        [Route("{id}")]
        public ActionResult<Product> GetById(int id)
        {
            if (id <= 0)
            {
                return NotFound("Id do produto deve ser maior que zero.");
            }
            Product product = _productContext.Products.FirstOrDefault(s => s.ProductId == id);
            if (product == null)
            {
                return NotFound("Produto não encontrado");
            }
            return Ok(product);
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody]Product product)
        {
            if (product == null)
            {
                return NotFound("Formato não suportado.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _productContext.Products.AddAsync(product);
            await _productContext.SaveChangesAsync();
            return Ok(product);
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody]Product product)
        {
            if (product == null)
            {
                return NotFound("Formato não suportado.");
            }
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Product existingProduct = _productContext.Products.FirstOrDefault(s => s.ProductId == product.ProductId);
            if (existingProduct == null)
            {
                return NotFound("Produto não encontrado.");
            }
            existingProduct.Name = product.Name;
            existingProduct.Price = product.Price;
            existingProduct.Image = product.Image;
            await _productContext.SaveChangesAsync();
            return Ok(existingProduct);
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound("Id não encontrado.");
            }
            Product product = _productContext.Products.FirstOrDefault(s => s.ProductId == id);
            if (product == null)
            {
                return NotFound("Nenhum produto encontrado com a Id informada.");
            }
            _productContext.Products.Remove(product);
            await _productContext.SaveChangesAsync();
            return Ok(new { success = true, msg = "Produto deletado com sucesso." });
        }

        ~ProductController()
        {
            _productContext.Dispose();
        }

    }
}
